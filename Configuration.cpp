//
// Created by zbybee on 4/13/2017.
//
#include "Configuration.h"

std::string Configuration::getValueByNameAsString(std::string str)
{
    dictionary.at(str);
}
int Configuration::getValueByNameAsInt(std::string str)
{
    return std::stoi(dictionary.at(str));
}
double Configuration::getValueByNameAsDouble(std::string str)
{
    return std::stod(dictionary.at(str));
}
void Configuration::setNewConfiguration(std::string key,std::string value)
{
    dictionary.emplace(key,value);//.emplace(key,value);
}
void Configuration::removeConfigurationByKey(std::string key)
{
    dictionary.erase(key);
}

Configuration::Configuration(const Configuration& config)
{
    this->dictionary = config.dictionary;
}
void Configuration::operator=(const Configuration& config)
{
    this->dictionary = config.dictionary;
}

Configuration::Configuration()
{

}

int Configuration::getConfigParamsCount()
{
    return dictionary.size();
}

