//
// Created by zbybee on 4/16/2017.
//

#include "DenialOfServiceAnalyzer.h"

DenialOfServiceAnalyzer::DenialOfServiceAnalyzer()
{

}

DenialOfServiceAnalyzer::~DenialOfServiceAnalyzer()
{

}

ResultSet DenialOfServiceAnalyzer::Run(std::iostream stream) {
    ResultSet set;
    if (!CheckConfigurationParameters()) {
        throw std::out_of_range("All Configruation Parameters were not found in correct format or existance");
    }
    return RunPhase1();
}

ResultSet DenialOfServiceAnalyzer::RunPhase1()
{
    ResultSet rs;
    bool isFirst = true;
    LogLine* LogLinePtr;
    std::vector<LogLine>* vctLogLines = &this->getVectorOfLogLines();
    std::string s;
    int c;
    for(int i =0; i < _AddressToSummary.size(); i++)//TODO: must get every line after the one checked within threashhold, and add it.
    {
        //LogLinePtr = &vctLogLines->at(i);
            std::map<std::string,int>* map = _AddressToSummary.at(LogLinePtr->getSrcAddress());
            s = LogLinePtr->getTimeStamp();
            c = map->at(s);
            c += ComputeTotalMessages(i,std::stoi(s)+timeFrame);
            bindResultSet(rs,c,LogLinePtr,isFirst);
            isFirst=false;
    }
    return rs;


}

void DenialOfServiceAnalyzer::bindResultSet(ResultSet& rs,int c,LogLine* logline,bool isFirst)
{

    if(c>= likelyThreashold)
    {
        rs.addResult("Likly Attackers",logline->getSrcPort());
    }
    else if(c>= possibleThreashold)
    {
        rs.addResult("Possible Attackers",logline->getSrcPort());
    }

}

int DenialOfServiceAnalyzer::ComputeTotalMessages(int currentMapIndex,int endPoint)
{
    int total = 0;
    std::vector<LogLine> *vctLogLines = &this->getVectorOfLogLines();
    for(int i =currentMapIndex; i < _AddressToSummary.size(); i++)
    {

        LogLine *LogLinePtr = &vctLogLines->at(i);
        std::map<std::string, int>* map = _AddressToSummary.at(LogLinePtr->getSrcAddress());
        if(std::stoi(LogLinePtr->getTimeStamp()) <= endPoint)
        {
            total += map->at(LogLinePtr->getTimeStamp());
        }
        else
        {
            break;
        }

    }
    return total;
}

bool DenialOfServiceAnalyzer::CheckConfigurationParameters()
{
        Configuration config = getConfiguration();
        timeFrame = config.getValueByNameAsInt("TimeFrame");//Automatically throws excpetion if does not exist.
        likelyThreashold = config.getValueByNameAsInt("LikelyAttackMessageCount");
        possibleThreashold = config.getValueByNameAsInt("PossibleAttackMessageCount");
}

void DenialOfServiceAnalyzer::load(std::string fileName,char delimiter) {
    Analyzer::load(fileName, delimiter);
    LogLine *LogLinePtr;
    std::vector<LogLine> *vctLogLines = &this->getVectorOfLogLines();
    for (int i = 0; i < vctLogLines->size(); i++) {
        LogLinePtr = &vctLogLines->at(i);
        bool doesExist = false;
        try {
            auto x = _AddressToSummary.at(LogLinePtr->getSrcAddress());
            _AddressToSummary.at(LogLinePtr->getSrcAddress())->at(LogLinePtr->getTimeStamp())++;
        }

        catch (std::out_of_range ex) {
            std::map<std::string, int> *map = new std::map<std::string, int>();
            map->emplace(LogLinePtr->getTimeStamp(), 1);
            _AddressToSummary.emplace(LogLinePtr->getSrcAddress(), map);
            vctDistinctSourceIP.push_back(LogLinePtr->getSrcAddress());
            //_AddressToSummary.at(LogLinePtr->getSrcAddress())->at(LogLinePtr->getTimeStamp())++;
        }
    }
}


