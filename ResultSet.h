//
// Created by zbybee on 4/13/2017.
//

#ifndef HW7ANALYZER_RESULTSET_H
#define HW7ANALYZER_RESULTSET_H

#include <iostream>
#include "IPrintable.h"

#include "Dictionary.h"
#include <vector>
#include <map>
class ResultSet : public IPrintable
{
public:
    void print(std::ostream&);
    void addResult(std::string,std::string value);
    std::vector<std::string> getResult(std::string key);
    void addResult(std::string key,std::vector<std::string> value);
    void print(std::ostream& os,std::string str);
private:
    std::map<std::string,std::vector<std::string>> dic;
};

#endif //HW7ANALYZER_RESULTSET_H
