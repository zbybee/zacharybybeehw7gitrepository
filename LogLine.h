//
// Created by zbybee on 4/16/2017.
//

#ifndef HW7ANALYZER_LOGLINE_H
#define HW7ANALYZER_LOGLINE_H
//
// Created by zbybee on 4/16/2017.
//

#include "LogLine.h"
#include <string>
class LogLine
{
public:
    LogLine();
    std::string getTimeStamp();
    std::string getSrcAddress();
    std::string getSrcPort();
    std::string getDesPort();
    void setTimeStamp(std::string str);
    void setSrcAddress(std::string str);
    void setSrcPort(std::string str);
    void setDesPort(std::string str);
    //bool isEqual(LogLine);
private:
    std::string _timeStamp;
    std::string _srcAddress;
    std::string _srcPort;
    std::string _desPort;
};
#endif //HW7ANALYZER_LOGLINE_H
