//
// Created by zbybee on 4/12/2017.
//

#ifndef HW7ANALYZER_ANALYZER_H
#define HW7ANALYZER_ANALYZER_H

#include "ResultSet.h"
#include "Configuration.h"
#include "iostream"
#include "LogLine.h"
#include "ILoadable.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
class Analyzer : public ILoadable
{
public:
    Analyzer();
    virtual ~Analyzer();
    virtual ResultSet Run(std::iostream stream) = 0;
    virtual void load(std::string fileName,char delimiter);
    virtual void SetConfiguration(Configuration config);
protected:
    std::vector<LogLine>& getVectorOfLogLines();
    virtual Configuration& getConfiguration();
    virtual bool CheckConfigurationParameters() = 0;
private:
    LogLine getLogLine(std::string logline,char delimiter);
    Configuration config;
    std::vector<LogLine> vctLogLines;
};


#endif //HW7ANALYZER_ANALYZER_H
