//
// Created by zbybee on 4/13/2017.
//

#ifndef HW7ANALYZER_CONFIGURATION_H
#define HW7ANALYZER_CONFIGURATION_H

#include <map>
class Configuration
{
public:
    Configuration();
    Configuration(const Configuration& config);
    void operator=(const Configuration& config);
    std::string getValueByNameAsString(std::string);
    int getValueByNameAsInt(std::string);
    double getValueByNameAsDouble(std::string);
    void setNewConfiguration(std::string key,std::string value);
    void removeConfigurationByKey(std::string key);
    int getConfigParamsCount();
private:
    std::map<std::string,std::string> dictionary;
    //Dictionary<std::string,std::string> dictionary;
};
#endif //HW7ANALYZER_CONFIGURATION_H
