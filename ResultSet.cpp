//
// Created by zbybee on 4/14/2017.
//
#include "ResultSet.h"

void ResultSet::print(std::ostream& os,std::string str)
{
    os << str;
}


void ResultSet::print(std::ostream& out)
{
    std::string outString = "";
        for(auto const& vctStr: dic)
        {
            outString = "Key:   \"" + vctStr.first + "\"\n";

                for(unsigned int ii=0; ii < vctStr.second.size(); ii++)
                {
                    outString += "Value:   " + vctStr.second.at(ii) + "\n";
                }

        }
    print(out,outString);
}

void ResultSet::addResult(std::string key,std::string value)
{

    //std::vector<std::string> vctString;
   // vctString.push_back(value);
    //dic.add(key,vctString);
}

void ResultSet::addResult(std::string key,std::vector<std::string> value)
{
    dic.emplace(key,value);
}

std::vector<std::string> ResultSet::getResult(std::string key)
{
    return dic.at(key);
}