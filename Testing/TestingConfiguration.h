//
// Created by zbybee on 4/22/2017.
//

#ifndef HW7ANALYZER_TESTINGCONFIGURATION_H
#define HW7ANALYZER_TESTINGCONFIGURATION_H

#include <iostream>
#include <exception>
#include "../Configuration.h"
#include "../ILoadable.h"
#include "../DenialOfServiceAnalyzer.h"
class TestingConfiguration{
public:
    void TestConfiguration();
};
#endif //HW7ANALYZER_TESTINGCONFIGURATION_H
