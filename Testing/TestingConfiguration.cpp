//
// Created by zbybee on 4/22/2017.
//
#include "TestingConfiguration.h"

void TestingConfiguration::TestConfiguration()
{
    std::cout <<"Testing configuration\n";
    Configuration config;

    config.setNewConfiguration("config1","configs");
    config.setNewConfiguration("config2Number","123");
    config.setNewConfiguration("config3","143");

    std::cout <<"Getting value as string:\n";

    if(config.getValueByNameAsString("config1") != "configs")
    {
        std::cout << "Value returned was not as expected.\n";
    }

    std::cout <<"Getting value as int:\n";
    if(config.getValueByNameAsInt("config3") != 123)
    {
        std::cout << "Value returned was supposed to be an int, but was not.\n";
    }

    std::cout <<"Getting value as double:\n";
    if(config.getValueByNameAsDouble("config3") != 123)
    {
        std::cout << "Value returned was supposed to be an double, but was not.\n";
    }

    std::cout <<"Removing value:\n";
    try
    {
        config.removeConfigurationByKey("config1");
        config.getValueByNameAsString("config1");
        std::cout << "Value returned was returend, but was not suppoesd to be returened.\n";
    }
    catch (std::out_of_range ex)
    {

    }


}