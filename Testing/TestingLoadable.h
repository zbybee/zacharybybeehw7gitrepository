//
// Created by zbybee on 4/22/2017.
//

#ifndef HW7ANALYZER_TESTINGLOADABLE_H
#define HW7ANALYZER_TESTINGLOADABLE_H

#include "../Configuration.h"
#include "../ILoadable.h"
#include "../DenialOfServiceAnalyzer.h"
#include <iostream>

class TestingLoadable
{
public:
    void TestLoadable();
};
#endif //HW7ANALYZER_TESTINGLOADABLE_H
