#include "LogLine.h"

LogLine::LogLine()
{
    _timeStamp = "";
    _desPort = "";
    _srcAddress = "";
    _srcPort = "";
}
std::string LogLine::getTimeStamp()
{
    return _timeStamp;
}
std::string LogLine::getSrcAddress()
{
    return _srcAddress;
}
std::string LogLine::getSrcPort()
{
    return  _srcPort;
}
std::string LogLine::getDesPort()
{
    return _desPort;
}
void LogLine::setTimeStamp(std::string str)
{
    _timeStamp = str;
}
void LogLine::setSrcAddress(std::string str)
{
    _srcAddress = str;
}
void LogLine::setSrcPort(std::string str)
{
    _srcPort = str;
}
void LogLine::setDesPort(std::string str)
{
    _desPort = str;
}
