//
// Created by zbybee on 4/16/2017.
//
#include "Analyzer.h"

Analyzer::Analyzer()
{

}

Analyzer::~Analyzer()
{
}

void Analyzer::load(std::string fileName,char delimiter)
{
        std::ifstream data(fileName);
        std::string logLine;
        while(std::getline(data,logLine))
        {
            vctLogLines.push_back(getLogLine(logLine,delimiter));
        }
}

LogLine Analyzer::getLogLine(std::string logline,char delimiter)
{
    LogLine log;
    std::stringstream lineStream(logline);
    std::string cell;
    std::getline(lineStream,cell,delimiter);
    log.setTimeStamp(cell);
    std::getline(lineStream,cell,delimiter);
    log.setSrcAddress(cell);
    std::getline(lineStream,cell,delimiter);
    log.setSrcPort(cell);
    std::getline(lineStream,cell,delimiter);
    log.setDesPort(cell);
    return log;
}

std::vector<LogLine>& Analyzer::getVectorOfLogLines()
{
    return vctLogLines;
}

void Analyzer::SetConfiguration(Configuration _config)
{
    config = _config;
}

Configuration& Analyzer::getConfiguration()
{
    return config;
}


