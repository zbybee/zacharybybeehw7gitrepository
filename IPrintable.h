//
// Created by zbybee on 4/14/2017.
//

#ifndef HW7ANALYZER_IPRINTABLE_H
#define HW7ANALYZER_IPRINTABLE_H

#include <iostream>
class IPrintable
{
public:
    virtual void print(std::ostream& os,std::string str) = 0;
};
#endif //HW7ANALYZER_IPRINTABLE_H
