//
// Created by zbybee on 4/10/2017.
//

#ifndef HW7_ILOADABLE_H
#define HW7_ILOADABLE_H

#include <iostream>
#include <vector>
//#include <istream>

class ILoadable
{
public:
    virtual void load(std::string fileName,char delimiter) = 0;
    //virtual std::vector<std::vector<std::string>*>* getVectorOfLogLines(char delimiter) = 0;
};


#endif //HW7_ILOADABLE_H
