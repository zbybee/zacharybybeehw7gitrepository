//
// Created by zbybee on 4/16/2017.
//

#ifndef HW7ANALYZER_DENIALOFSERVICEANALYZER_H
#define HW7ANALYZER_DENIALOFSERVICEANALYZER_H

#include "Analyzer.h"
#include "Configuration.h"
#include "Dictionary.h"
#include "KeyValue.h"
#include <string>
#include <map>
#include <exception>
#include <algorithm>
class DenialOfServiceAnalyzer : public Analyzer
{
public:
    DenialOfServiceAnalyzer();
    virtual ~DenialOfServiceAnalyzer();
    virtual ResultSet Run(std::iostream stream);
    virtual void load(std::string fileName,char delimiter);

protected:
    virtual bool CheckConfigurationParameters();
private:
    void bindResultSet(ResultSet&,int c,LogLine* logLine,bool isTrue);
    int timeFrame;
    int likelyThreashold;
    int possibleThreashold;
    ResultSet RunPhase1();
    int ComputeTotalMessages(int currentMapIndex,int);
    //bool checkIfLogExists(std::vector<LogLine>& vctLogLines, LogLine& LogLinePtr);
    std::map<std::string,std::map<std::string,int>*> _AddressToSummary;
    std::vector<std::string> vctDistinctSourceIP;
    //Dictionary<std::string,Dictionary<std::string,int>*> _AddressToSummary;
    ///Dictionary<std::string,int> _TimeStampToCount;
};
#endif //HW7ANALYZER_DENIALOFSERVICEANALYZER_H
